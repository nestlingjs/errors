import {
  DynamicModule,
  Global,
  Module,
} from '@nestjs/common'
import { HttpExceptionFilter } from './HttpExceptionFilter'

export function createErrorProvider() {
  return {
    provide: HttpExceptionFilter,
    useFactory: () => new HttpExceptionFilter()
  }
}
@Global()
@Module({})
export class ErrorModule {
  static forRoot(): DynamicModule {
    const provider = createErrorProvider()

    return {
      module: ErrorModule,
      providers: [provider],
      exports: [provider]
    }
  }
}
