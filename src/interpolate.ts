const regExp = /{([^{}]*)}/g

export const interpolate = <T = object>(message: string) =>
  (o: T) => message
    .replace(regExp, (a, b) => o[b])
