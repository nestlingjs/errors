import {
  ArgumentsHost,
  ExceptionFilter,
  HttpStatus,
  Catch
} from '@nestjs/common'
import {
  errorMessageHandler,
  httpExceptionHandler }
  from './handlers'
import { Response } from 'express'
import { IErrorMessage } from './ErrorMessage'
import * as isInstanceOf from 'is-instance-of'

export type ErrorResponseHandlerType =
  (
    error: IErrorMessage,
    response: Response,
    exception: Error
  ) => {}

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
  static exceptionHandlers = new Map()
  static errorResponseHandler: ErrorResponseHandlerType

  static setErrorResponseHandler (fn) {
    HttpExceptionFilter.errorResponseHandler = fn
  }

  static addExceptionHandler (handler) {
    HttpExceptionFilter.exceptionHandlers.set(handler.type, handler.handler)
  }

  public catch (
    exception: Error,
    host: ArgumentsHost
  ) {
    let error: IErrorMessage
    const ctx = host.switchToHttp()
    const response = ctx.getResponse<Response>()

    for (let [type, handler] of HttpExceptionFilter.exceptionHandlers) {
      if (
        exception instanceof type ||
        isInstanceOf(exception, type) ||
        exception.name === type.constructor.name
      ) {
        error = handler(exception)

        break;
      }
    }

    if (!error) {
      error = {
        code: 'bad_request',
        message: 'Internal Server Error',
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR
      }
    }

    HttpExceptionFilter.errorResponseHandler(error, response, exception)
  }
}

HttpExceptionFilter.addExceptionHandler(httpExceptionHandler)
HttpExceptionFilter.addExceptionHandler(errorMessageHandler)
/**
 * Sends out the error response.
 *
 * Override this to control what is send back to the client.
 *
 * And possibly include your own logging.
 */
HttpExceptionFilter.setErrorResponseHandler((
  error: IErrorMessage,
  response: Response,
  exception: Error
) => {
  response.setHeader(`x-error-code`, error.code)
  response.setHeader(`x-error-message`, error.message)
  response.setHeader(`x-error-statusCode`, error.statusCode)
  response.setHeader('Content-Type', 'application/json')
  response
    .status(error.statusCode)
    .json(error)

  console.error(exception)
  console.error(error)
})
