import { HttpStatus } from '@nestjs/common'
import { interpolate } from './interpolate'

export interface IErrorMessage {
  code: string
  message: string
  statusCode: HttpStatus
}

export interface IErrorMessages {
  type: string
  errors: IErrorMessage[]
}

export class ErrorMessage <TVars = any> extends Error {
  public code: string
  public statusCode: number
  static errorCodes = {}

  constructor (messageCode: string, vars?: TVars) {
    super()

    const errorMessage = ErrorMessage.errorCodes[messageCode]

    if (!errorMessage) {
      throw Error('Could not find errorMessage')
    }

    Error.captureStackTrace(this, this.constructor)
    this.name = this.constructor.name
    this.statusCode = errorMessage.statusCode
    this.code = messageCode
    if (vars) {
      this.message = interpolate<TVars>(errorMessage.message)(vars)
    } else {
      this.message = errorMessage.message
    }
  }

  static addErrorMessages(errorMessages) {
    errorMessages.errors.reduce((codes, error) => {
      codes[`${errorMessages.type}:${error.code}`] = error

      return codes
    }, ErrorMessage.errorCodes)
  }
}
