import * as jsonwebtoken from 'jsonwebtoken'
import { HttpStatus } from '@nestjs/common'
import { IErrorMessage } from '../ErrorMessage'

export const jsonWebTokenErrorHandler = {
  type: jsonwebtoken.JsonWebTokenError,
  handler: (exception): IErrorMessage => {
    return {
      code: exception.name,
      message: exception.message,
      statusCode: HttpStatus.BAD_REQUEST
    }
  }
}
