import { HttpException } from '@nestjs/common'
import { IErrorMessage } from '../ErrorMessage'

export const httpExceptionHandler = {
  type: HttpException,
  handler: (exception): IErrorMessage => {
    return {
      code: exception.response.error,
      message: exception.response.message,
      statusCode: exception.response.statusCode
    }
  }
}
