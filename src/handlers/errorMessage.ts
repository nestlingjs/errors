import { ErrorMessage, IErrorMessage } from '../ErrorMessage'

export const errorMessageHandler = {
  type: ErrorMessage,
  handler: (exception): IErrorMessage => {
    return {
      code: exception.code,
      message: exception.message,
      statusCode: exception.statusCode
    }
  }
}
