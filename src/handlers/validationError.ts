import { HttpStatus } from '@nestjs/common'
import { IErrorMessage } from '../ErrorMessage'
const Ajv = require('ajv')

const { ValidationError } = Ajv

export const validationErrorHandler = {
  type: ValidationError,
  handler: (exception): IErrorMessage => {
    return {
      code: exception.errors[0].keyword,
      message: exception.errors[0].message,
      statusCode: HttpStatus.BAD_REQUEST
    }
  }
}
