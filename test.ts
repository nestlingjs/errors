import { interpolate } from './src/interpolate'

console.log(interpolate('This is my message: {message}')({ message: 'my message!' }))
console.log(interpolate('This is my message: {message}')({ heh: 'heh!' }))
